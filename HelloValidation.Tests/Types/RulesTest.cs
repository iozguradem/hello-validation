using System.Collections.Generic;
using System.Linq;
using HelloValidation.Types;
using Xunit;

namespace HelloValidation.Tests.Types
{

    public class RulesTest
    {   

        [Fact]
        public void RequiredTest()
        {
            Rules rules = new Rules() { required = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateRequired("foo", null, errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Null(errors.FirstOrDefault().fieldValue);
            Assert.Equal("required", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field is required.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateRequired("foo", "bar", errors));
            Assert.Single(errors);

            rules = new Rules();
            Assert.True(rules.ValidateRequired("foo", null, errors));
        }

        [Fact]
        public void EmailTest()
        {
            Rules rules = new Rules() { email = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.True(rules.ValidateEmail("foo", null, errors));
            Assert.False(rules.ValidateEmail("foo", "bar", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar", errors.FirstOrDefault().fieldValue);
            Assert.Equal("email", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must contain a valid email address.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateEmail("foo", "foo@bar.com", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void MinLengthTest()
        {
            Rules rules = new Rules() { minLength = 3 };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateMinLength("foo", "a", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("a", errors.FirstOrDefault().fieldValue);
            Assert.Equal("minLength", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must be at least 3 characters in length.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateMinLength("foo", "bar", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void MaxLengthTest()
        {
            Rules rules = new Rules() { maxLength = 3 };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateMaxLength("foo", "barbar", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("barbar", errors.FirstOrDefault().fieldValue);
            Assert.Equal("maxLength", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field can not exceed 3 characters in length.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateMaxLength("foo", "bar", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void ExactLengthTest()
        {
            Rules rules = new Rules() { exactLength = 6 };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateExactLength("foo", "bar", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar", errors.FirstOrDefault().fieldValue);
            Assert.Equal("exactLength", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must be exactly 6 characters in length.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateExactLength("foo", "barbar", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void AlphaTest()
        {
            Rules rules = new Rules() { alpha = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateAlpha("foo", "bar123", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar123", errors.FirstOrDefault().fieldValue);
            Assert.Equal("alpha", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field may only contain alphabetical characters.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateAlpha("foo", "bar", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void AlphaDashTest()
        {
            Rules rules = new Rules() { alphaDash = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateAlphaDash("foo", "bar123??", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar123??", errors.FirstOrDefault().fieldValue);
            Assert.Equal("alphaDash", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field may only contain alpha-numeric characters, underscores, and dashes.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateAlphaDash("foo", "bar-bar", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void AlphaNumericTest()
        {
            Rules rules = new Rules() { alphaNumeric = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateAlphaNumeric("foo", "bar-123", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar-123", errors.FirstOrDefault().fieldValue);
            Assert.Equal("alphaNumeric", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field may only contain alpha-numeric characters.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateAlphaNumeric("foo", "bar123", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void NumericTest()
        {
            Rules rules = new Rules() { numeric = true };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateNumeric("foo", "bar123", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar123", errors.FirstOrDefault().fieldValue);
            Assert.Equal("numeric", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must contain only numbers.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateNumeric("foo", "123", errors));
            Assert.Single(errors);
        }

        [Fact]
        public void MinValueTest()
        {
            Rules rules = new Rules() { minValue = 18 };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateMinValue("foo", 17, errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal(17, errors.FirstOrDefault().fieldValue);
            Assert.Equal("minValue", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must contain a number greater than 18.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateMinValue("foo", 18, errors));
            Assert.Single(errors);
        }

        [Fact]
        public void MaxValueTest()
        {
            Rules rules = new Rules() { maxValue = 18 };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateMaxValue("foo", 19, errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal(19, errors.FirstOrDefault().fieldValue);
            Assert.Equal("maxValue", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field must contain a number less than 18.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateMaxValue("foo", 17, errors));
            Assert.Single(errors);
        }

        [Fact]
        public void RegexTest()
        {
            Rules rules = new Rules() { regex = "^[0-9]*$" };
            List<ValidationError> errors = new List<ValidationError>();
            Assert.False(rules.ValidateRegex("foo", "bar", errors));
            Assert.Single(errors);
            Assert.Equal("foo", errors.FirstOrDefault().fieldName);
            Assert.Equal("bar", errors.FirstOrDefault().fieldValue);
            Assert.Equal("regex", errors.FirstOrDefault().rule);
            Assert.Equal("The foo field is not in the correct format.", errors.FirstOrDefault().message);
            Assert.True(rules.ValidateRegex("foo", 17, errors));
            Assert.Single(errors);
        }

    }

}