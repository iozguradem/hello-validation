using System.Collections.Generic;
using HelloValidation.Exceptions;
using HelloValidation.Types;
using Xunit;

namespace HelloValidation.Tests
{
    public class MyData
    {
        public string username { get; set; }
    }

    public class ValidatorTest
    {

        [Fact]
        public void YamlTest()
        {
            Validator validator = new Validator();
            MyData data = new MyData() { username = "" };
            string configuration = TestHelper.ReadYamlData("04");
            Assert.False(validator.IsValid(data, configuration));
            Assert.True(validator.IsNotValid(data, configuration));
        }

        [Fact]
        public void ObjectTest()
        {
            Validator validator = new Validator();
            MyData data = new MyData() { username = "" };
            Dictionary<string, Rules> configuration = new Dictionary<string, Rules>()
            {
                { "username", new Rules () { required = true }}
            };
            Assert.False(validator.IsValid(data, configuration));
            Assert.True(validator.IsNotValid(data, configuration));
        }

        [Fact]
        public void ExceptionTest()
        {
            Validator validator = new Validator();
            MyData data = new MyData() { username = "" };
            string configuration = TestHelper.ReadYamlData("04");
            Assert.Throws<ValidationException>(() => validator.Validate(data, configuration));
        }

        [Fact]
        public void ErrorCountTest()
        {
            Validator validator = new Validator();
            MyData data = new MyData() { username = "" };
            string configuration = TestHelper.ReadYamlData("04");
            validator.IsValid(data, configuration);
            Assert.Equal(2, validator.Errors().Count);
        }

        [Fact]
        public void DictionaryFailTest()
        {
            Validator validator = new Validator();
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
            {
                { "username", null }
            };
            string configuration = TestHelper.ReadYamlData("04");
            Assert.False(validator.IsValid(data, configuration));
        }

        [Fact]
        public void DictionarySuccessTest()
        {
            Validator validator = new Validator();
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
            {
                { "username", "foobar" }
            };
            string configuration = TestHelper.ReadYamlData("04");
            Assert.True(validator.IsValid(data, configuration));
        }

    }
}