using Xunit;

namespace HelloValidation.Tests
{
    public class ResultCollectorTest
    {

        [Fact]
        public void Test()
        {
            ResultCollector collector = new ResultCollector();
            Assert.True(collector.GetResult());

            collector.Add(true);
            Assert.True(collector.GetResult());

            collector.Add(false);
            Assert.False(collector.GetResult());

            collector.Add(true);
            Assert.False(collector.GetResult());
        }

    }
}