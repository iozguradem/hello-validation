using System;
using System.Collections.Generic;
using HelloValidation.Exceptions;
using HelloValidation.Helpers;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace HelloValidation.Tests.Helpers
{

    public class MyData
    {
        public string username { get; set;}
        public string password { get; set; }
        public int? age { get; set; }
        public double salary { get; set; }

    }

    public class DataHelperTest
    {
        private ITestOutputHelper output { get; set; }

        public DataHelperTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void GetValueTest()
        {
            Assert.Equal("foo", DataHelper.GetValue(new MyData() { username = "foo" }, "username"));
            Assert.Null(DataHelper.GetValue(new MyData(), "username"));
            Assert.Equal(12, DataHelper.GetValue(new MyData() { age = 12 }, "age"));
            Assert.Null(DataHelper.GetValue(new MyData(), "age"));
            Assert.Equal(666.66, DataHelper.GetValue(new MyData() { salary = 666.66 }, "salary"));
            Assert.Null(DataHelper.GetValue(null, "username"));
        }

        [Fact]
        public void GetValueExeptionTest()
        {
            Assert.Throws<UndefinedFieldException>(() => DataHelper.GetValue(new MyData(), "xxx"));
        }

        [Fact]
        public void DictionaryTest()
        {
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>()
            {
                { "username", "foo" }
            };

            Assert.Equal("foo", DataHelper.GetValue(data, "username"));
        }


    }
}