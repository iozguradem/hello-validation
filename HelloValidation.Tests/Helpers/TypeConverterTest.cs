using System.Collections.Generic;
using System.IO;
using HelloValidation.Helpers;
using HelloValidation.Types;
using Xunit;

namespace HelloValidation.Tests.Helpers
{

    public class TypeConverterTest
    {

        [Fact]
        public void ToConfigurationTest()
        {
            string content = TestHelper.ReadYamlData("01");
            Dictionary<string, Rules> response = TypeConverter.ToConfiguration(content);
            Assert.True(response.ContainsKey("username"));
            Rules rule = response["username"];
            Assert.True(rule.required);
            Assert.True(rule.email);
            Assert.Equal(1, rule.minLength); 
            Assert.Equal(2, rule.maxLength); 
            Assert.Equal(3, rule.exactLength); 
            Assert.True(rule.alpha);
            Assert.True(rule.alphaNumeric);
            Assert.True(rule.alphaDash);
            Assert.True(rule.numeric);
            Assert.Equal(4, rule.maxValue); 
            Assert.Equal(5, rule.minValue);
            Assert.Equal("^[0-9]*$", rule.regex);
        }

        [Fact]
        public void ToConfigurationDefaultsTest()
        {
            string content = TestHelper.ReadYamlData("02");
            Dictionary<string, Rules> response = TypeConverter.ToConfiguration(content);
            Assert.True(response.ContainsKey("username"));
            Rules rule = response["username"];
            Assert.True(rule.required);
            Assert.False(rule.email);
            Assert.Null(rule.minLength); 
            Assert.Null(rule.maxLength); 
            Assert.Null(rule.exactLength); 
            Assert.False(rule.alpha);
            Assert.False(rule.alphaNumeric);
            Assert.False(rule.alphaDash);
            Assert.False(rule.numeric);
            Assert.Null(rule.maxValue); 
            Assert.Null(rule.minValue);
            Assert.Null(rule.regex);
        }

        [Fact]
        public void ToConfigurationMultipleTest()
        {
            string content = TestHelper.ReadYamlData("03");
            Dictionary<string, Rules> response = TypeConverter.ToConfiguration(content);
            Assert.Equal(4, response.Count);
        }        

    }

}