using System.IO;

namespace HelloValidation.Tests
{

    public class TestHelper
    {

        public static string ReadYamlData(string filename)
        {
            return File.ReadAllText(
                Path.Combine(TestHelper.GetRootPath(), "data", filename + ".yml")
            );
        }

        public static string GetRootPath()
        {
            return Directory
                .GetCurrentDirectory()
                .Replace("bin/Debug/netcoreapp2.1", "")
                .Replace("bin\\Debug\\netcoreapp2.1", "");
        }

    }

}