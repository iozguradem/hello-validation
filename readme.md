# HelloValidation

HelloValidation is another validation library which can run with a configuration file for an object.

> This library is under development! Don't use it on production.

## Installation

// TODO 

## Usage

You can use this data validator in two diffeent ways; 

- Configuration object
- Configuration string (Yaml)

In following codes you may see the configuration object usage;

```csharp
MyData data = new MyData()
{
    username = "username",
    password = "123",
    email = null,
    age = 1234
};

Dictionary<string, IRules> configuration = new Dictionary<Rules>()
configuration.Add(username, new Rules() 
{
    isRequired = true,
    maxLength = 30,
    minLength = 4
});

configuration.Add(password, new Rules() 
{
    isRequired = true,
    maxLength = 30,
    minLength = 8
});

configuration.Add(email, new Rules() 
{
    isRequired = true
});

configuration.Add(age, new Rules() 
{
    minValue = 18,
    maxValue = 100
});

Validator validator = new Validator();
validator.Validate(data, configuration);
```

In following codes you may see the configuration string (Yaml) usage;

```csharp
MyData data = new MyData()
{
    username = "username",
    password = "123",
    email = null,
    age = 1234
};

string configuration = @"
username: 
    isRequired: true
    maxLength: 30
    minLength: 4
password:
    isRequired: true
    maxLength: 30
    minLength: 8
email:
    isRequired: true
age:
    minValue: 18
    maxValue: 100
"

Validator validator = new Validator();
validator.Validate(data, configuration);
```

## Methods 

### Validate

Validate method has two different types;

```csharp
void Validate(object data, Dictionary<string, Rules> configuration)
{
    // 
}

void Validate(object data, string configuration)
{
    // 
}
```

Example usage;

```csharp
Validator validator = new Validator();
validator.Validate(data, configuration);
```

If the data is valid, the code continuous to execution. If it is not, you get an exception; `ValidationException` 

### IsValid

Validate method has two different types;

```csharp
bool IsValid(object data, Dictionary<string, Rules> configuration)
{
    // 
}

bool IsValid(object data, string configuration)
{
    // 
}
```

Example usage;

```csharp
Validator validator = new Validator();
if (validator.IsValid(data, configuration))
{
    // something...
}
```

### IsNotValid

Validate method has two different types;

```csharp
bool IsNotValid(object data, Dictionary<string, Rules> configuration)
{
    // 
}

bool IsNotValid(object data, string configuration)
{
    // 
}
```

Example usage;

```csharp
Validator validator = new Validator();
if (validator.IsNotValid(data, configuration))
{
    // something...
}
```

### Errors

You can use this method to get errors;

```csharp
List<ValidationError> Errors();
```

Example usage;

```csharp
Validator validator = new Validator();
if (validator.IsNotValid(data, configuration))
{
    List<ValidationError> errors = validator.Errors();
}
```

## Yaml Configuration

You may see the example of yaml configuration;

```yml
my_field_name: 
    required: true
    email: true
    minLength: 10
    maxLength: null
    exactLength: 10
    alpha: true
    alphaNumeric: true
    alphaDash: true
    numeric: true
    maxValue: 3.14
    minValue: 10000
    regex: ^[0-9]*$
```

## License

[GNU GENERAL PUBLIC LICENSE Version 3](LICENSE.md)