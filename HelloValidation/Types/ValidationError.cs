namespace HelloValidation.Types
{

    public class ValidationError
    {

        public string fieldName { get; set; }
        public dynamic fieldValue  { get; set; }
        public string rule { get; set; }
        public string message { get; set; }

    }

}