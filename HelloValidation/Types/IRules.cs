namespace HelloValidation.Types
{

    public interface IRules
    {
        bool required { get; set; }
        bool email { get; set; }
        int? minLength { get; set; }
        int? maxLength { get; set; }
        int? exactLength { get; set; }
        bool alpha { get; set; }
        bool alphaNumeric { get; set; }
        bool alphaDash { get; set; }
        bool numeric { get; set; }
        float? maxValue { get; set; }
        float? minValue { get; set; }
    }

}