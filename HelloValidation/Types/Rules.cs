using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace HelloValidation.Types
{

    public class Rules
    {
        public bool required { get; set; }
        public bool email { get; set; }
        public int? minLength { get; set; }
        public int? maxLength { get; set; }
        public int? exactLength { get; set; }
        public bool alpha { get; set; }
        public bool alphaNumeric { get; set; }
        public bool alphaDash { get; set; }
        public bool numeric { get; set; }
        public float? maxValue { get; set; }
        public float? minValue { get; set; }
        public string regex { get; set; }

        public bool ValidateRequired(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (required == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Length == 0)
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "required",
                    message = "The {0} field is required.".Replace("{0}", fieldName),
                });
                return false;
            }

            return true;
        }

        public bool ValidateEmail(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (email == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Length == 0)
            {
                return true;
            }

            bool isEmail = false;

            try {
                MailAddress addr = new MailAddress(fieldValue.ToString());
                isEmail = addr.Address == fieldValue.ToString();
            }
            catch {
                isEmail = false;
            }            

            if (isEmail == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "email",
                    message = "The {0} field must contain a valid email address.".Replace("{0}", fieldName),
                });
                return false;
            }

            return true;
        }

        public bool ValidateMinLength(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (minLength == null)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length < minLength)
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "minLength",
                    message = "The {0} field must be at least {1} characters in length."
                        .Replace("{0}", fieldName)
                        .Replace("{1}", minLength.ToString()),
                });
                return false;
            }

            return true;
        }

        public bool ValidateMaxLength(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (maxLength == null)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length > maxLength)
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "maxLength",
                    message = "The {0} field can not exceed {1} characters in length."
                        .Replace("{0}", fieldName)
                        .Replace("{1}", maxLength.ToString()),
                });
                return false;
            }

            return true;
        }

        public bool ValidateExactLength(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (exactLength == null)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length != exactLength)
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "exactLength",
                    message = "The {0} field must be exactly {1} characters in length."
                        .Replace("{0}", fieldName)
                        .Replace("{1}", exactLength.ToString()),
                });
                return false;
            }

            return true;
        }

        public bool ValidateAlpha(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (alpha == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            Regex regex = new Regex("^[a-zA-Z]*$");
            if (regex.IsMatch(fieldValue.ToString()) == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "alpha",
                    message = "The {0} field may only contain alphabetical characters.".Replace("{0}", fieldName),
                });
                return false;
            }            

            return true;            
        }

        public bool ValidateAlphaNumeric(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (alphaNumeric == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            Regex regex = new Regex("^[a-zA-Z0-9]*$");
            if (regex.IsMatch(fieldValue.ToString()) == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "alphaNumeric",
                    message = "The {0} field may only contain alpha-numeric characters.".Replace("{0}", fieldName),
                });
                return false;
            }

            return true;
        }

        public bool ValidateAlphaDash(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (alphaDash == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            Regex regex = new Regex("^[a-zA-Z0-9_-]*$");
            if (regex.IsMatch(fieldValue.ToString()) == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "alphaDash",
                    message = "The {0} field may only contain alpha-numeric characters, underscores, and dashes.".Replace("{0}", fieldName),
                });
                return false;
            }            

            return true;
        }

        public bool ValidateNumeric(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (numeric == false)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            Regex regex = new Regex("^[0-9]*$");
            if (regex.IsMatch(fieldValue.ToString()) == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "numeric",
                    message = "The {0} field must contain only numbers.".Replace("{0}", fieldName),
                });
                return false;
            }
            
            return true;
        }

        public bool ValidateMaxValue(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (maxValue == null)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            double value = Convert.ToDouble(fieldValue);
            if (value > maxValue) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "maxValue",
                    message = "The {0} field must contain a number less than {1}."
                        .Replace("{0}", fieldName)
                        .Replace("{1}", maxValue.ToString()),
                });
                return false;
            }
            
            return true;            
        }

        public bool ValidateMinValue(string fieldName, object fieldValue, List<ValidationError> errors)        
        {
            if (minValue == null)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            double value = Convert.ToDouble(fieldValue);
            if (value < minValue) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "minValue",
                    message = "The {0} field must contain a number greater than {1}."
                        .Replace("{0}", fieldName)
                        .Replace("{1}", minValue.ToString()),
                });
                return false;
            }
            
            return true;
        }

        public bool ValidateRegex(string fieldName, object fieldValue, List<ValidationError> errors)
        {
            if (regex == null || regex.Trim().Length == 0)
            {
                return true;
            }

            if (fieldValue == null || fieldValue.ToString().Trim().Length == 0)
            {
                return true;
            }

            Regex validator = new Regex(regex);
            if (validator.IsMatch(fieldValue.ToString()) == false) 
            {
                errors.Add(new ValidationError() {
                    fieldName = fieldName,
                    fieldValue  = fieldValue,
                    rule = "regex",
                    message = "The {0} field is not in the correct format.".Replace("{0}", fieldName),
                });
                return false;
            }
            
            return true;
        }        

    }

}