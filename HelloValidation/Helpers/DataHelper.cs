using System;
using System.Collections.Generic;
using System.Reflection;
using HelloValidation.Exceptions;
using Newtonsoft.Json;

namespace HelloValidation.Helpers
{

    public class DataHelper
    {
        
        public static object GetValue(object data, string property)
        {
            if (data == null)
            {
                return null;
            }

            if (IsDictionary(data))
            {
                return GetDictionaryData(
                    JsonConvert.DeserializeObject<Dictionary<dynamic, dynamic>>(
                        JsonConvert.SerializeObject(data)
                    ), 
                    property
                );
            }

            PropertyInfo propertyInfo = data.GetType().GetProperty(property);
            if (propertyInfo == null)
            {
                throw new UndefinedFieldException(property);
            }
            return propertyInfo.GetValue(data, null);
        }

        private static object GetDictionaryData(Dictionary<dynamic, dynamic> data, string property)
        {
            if (data.ContainsKey(property) == false)
            {
                throw new UndefinedFieldException(property);
            }

            return data[property];
        }

        private static bool IsDictionary(object data)
        {
            return data.GetType().IsGenericType && 
                data.GetType().
                GetGenericTypeDefinition() == typeof(Dictionary<,>);
        }

    }

}