using System.Collections.Generic;
using HelloValidation.Types;
using YamlDotNet.Serialization;

namespace HelloValidation.Helpers
{
    public class TypeConverter
    {

        public static Dictionary<string, Rules> ToConfiguration(string yaml)
        {
            Deserializer deserializer = new Deserializer();
            return deserializer.Deserialize<Dictionary<string, Rules>>(yaml);
        }

    }
}