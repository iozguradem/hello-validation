using System;
using System.Collections.Generic;
using HelloValidation.Exceptions;
using HelloValidation.Helpers;
using HelloValidation.Types;
using Newtonsoft.Json;

namespace HelloValidation
{

    public class Validator
    {

        private List<ValidationError> errors { get; set; }

        public bool IsValid(object data, Dictionary<string, Rules> configuration)
        {
            errors = new List<ValidationError>();
            ResultCollector collector = new ResultCollector();
            foreach (KeyValuePair<string, Rules> pair in configuration)
            {
                object value = DataHelper.GetValue(data, pair.Key);
                collector.Add(pair.Value.ValidateRequired(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateEmail(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateMinLength(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateMaxLength(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateExactLength(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateAlpha(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateAlphaNumeric(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateAlphaDash(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateNumeric(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateMaxValue(pair.Key, value, errors));
                collector.Add(pair.Value.ValidateMinValue(pair.Key, value, errors));
            }
            return collector.GetResult();
        }

        public bool IsValid(object data, string configuration)
        {
            return IsValid(data, TypeConverter.ToConfiguration(configuration));
        }

        public bool IsNotValid(object data, Dictionary<string, Rules> configuration)
        {
            return !IsValid(data, configuration);
        }

        public bool IsNotValid(object data, string configuration)
        {
            return !IsValid(data, configuration);
        }

        public void Validate(object data, Dictionary<string, Rules> configuration)
        {
            if (IsNotValid(data, configuration))
            {
                throw new ValidationException();
            }
        }

        public void Validate(object data, string configuration)
        {
            if (IsNotValid(data, configuration))
            {
                throw new ValidationException();
            }
        }

        public List<ValidationError> Errors()
        {
            return errors;
        }

    }

}