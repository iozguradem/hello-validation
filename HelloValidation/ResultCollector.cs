namespace HelloValidation
{

    public class ResultCollector
    {
        private bool result { get; set; }

        public ResultCollector()
        {
            result = true;
        }

        public void Add(bool newResult)
        {
            if (newResult == false)
            {
                result = false;
            }
        }

        public bool GetResult()
        {
            return result;
        }

    }

}