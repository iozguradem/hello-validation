using System;

namespace HelloValidation.Exceptions
{

    public class UndefinedFieldException: Exception
    {

        public UndefinedFieldException(string property): base("Undefined property: " + property)
        {
            
        }
        
    }

}